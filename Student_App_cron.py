import os
import datetime
from sendemail import sendemail
import requests
import json
import time
import sys
import glob

time_to_run = sys.argv[1]
dos_ = "AndroidP"
if sys.argv[2] == 'ios':
    dos_ = "IosP"


def readFile(file):
    with open(file, 'r') as f:
        data = f.read()
    return data


while True:
    date = datetime.datetime.now()
    time_now = date.strftime("%H:%M")
    if str(time_to_run) == str(time_now):
        os.system("sudo rm -r student-app-cross-platform-automation")

        url = "https://device.pcloudy.com/api/devices"
        drive_url = "https://device.pcloudy.com/api/drive"
        os.system("git clone git@bitbucket.org:microservicesembibe/student-app-cross-platform-automation.git")

        payload_drive_name = payload = json.dumps({
            "token": "k685qrtn7mnwqzdp5ccmprbg",
            "limit": 100,
            "filter": "all"
        })
        if sys.argv[2] == 'ios':
            payload_device_name = json.dumps({
                "token": "k685qrtn7mnwqzdp5ccmprbg",
                "duration": 1,
                "platform": "Ios",
                "available_now": "true"
            })
        else:
            payload_device_name = json.dumps({
                "token": "k685qrtn7mnwqzdp5ccmprbg",
                "duration": 1,
                "platform": "android",
                "available_now": "true"
            })

        headers = {
            'Connection': 'keep-alive',
            'Accept': 'application/json, text/plain, */*',
            'sec-ch-ua-mobile': '?0',
            'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/99.0.4844.51 Safari/537.36',
            'sec-ch-ua': '" Not A;Brand";v="99", "Chromium";v="99", "Google Chrome";v="99"',
            'sec-ch-ua-platform': '"macOS"',
            'Content-Type': 'application/json',
            'Origin': 'https://paas-v3-staging.embibe.com',
            'Sec-Fetch-Site': 'same-site',
            'Sec-Fetch-Mode': 'cors',
            'Sec-Fetch-Dest': 'empty',
            'Referer': 'https://paas-v3-staging.embibe.com/',
            'Accept-Language': 'en-US,en-GB;q=0.9,en;q=0.8,hi-IN;q=0.7,hi;q=0.6'
        }

        response_devices = requests.request("POST", url, headers=headers, data=payload_device_name)
        response_drive = requests.request("POST", drive_url, headers=headers, data=payload_drive_name)
        models_list = response_devices.json().get('result').get('models')

        model = models_list[len(models_list) - 1].get('full_name')
        filename = "EmbibeParentBeta_14012_Resigned1659000128.ipa"

        for apk_name in response_drive.json().get('result').get('files'):
            if sys.argv[2] == 'ios' and apk_name.get('file').endswith('ipa') and apk_name.get('file').startswith(
                    'Embibe28_Resigned'):
                filename = apk_name.get('file')
            elif apk_name.get('file').endswith('apk') and apk_name.get('file').startswith("StudentVersion"):
                filename = apk_name.get('file')

        vm = os.popen('uname -n').read()
        vmos = os.popen('lsb_release -d').read()

        body = "<html><body><p><center><font color='red'><i>*****This is an auto-generated " \
               "email*****</i></font></center></p><p>Hello, " + "</p><p>Please do not reply to this mail! The report " \
                                                                "for the test is attached in this email. To access it " \
                                                                "just download and open with any browser.</p><p>The " \
                                                                "details for the test are as follows :- </p><table " \
                                                                "border='1'> <tr> <th>Execution Request for:</th> " \
                                                                "<td>" + "Student " + dos_ + " App UI Automation" + \
               "</td> </tr> <tr> <th>Virtual Machine Used:</th> <td>" + vm + "</td> </tr> <tr> <th>Environment:</th> " \
                                                                             "<td>" + \
               "BETA" + "</td> </tr> <tr> <th>Test Suite:</th> <td>" + "BETA_SANITY-120" + "</td> </tr> <tr> <th>Device:</th> <td>" + model + "</td> </tr> <tr> <th>App Used:</th> <td>" + filename + "</td> </tr> </table><p><font color='blue'>Note: Contact QA Automation Team for any queries. You can reach us at automation-testing@embibe.com. Request came from OnDemand-Automation.</font></p></body></html>"

        print(dos_, filename, model)
        address = ["vipin.vishwakarma@embibe.com", "aliya.kumari@embibe.com", ]

        os.system(
            'cd student-app-cross-platform-automation/ && gradle clean SanityTests -Denv="' + "beta" + '" -Dos=' + (
                dos_) + ' -Dgroups="' + "BETA_SANITY" + '" -Dtime="' + '120' + '" -DappName="' + (
                filename) + '" -DdeviceName="' + (model) + '"')

        sendemail(", ".join(address), "Student App Ui Automation Report", body,
                  "student-app-cross-platform-automation/ExtendReport/",
                  "student-app-cross-platform-automation/ExtendReport/",
                  glob.glob('student-app-cross-platform-automation/ExtendReport/*.png'))
        print(f"mail sent at {datetime.datetime.now()}")
    time.sleep(10)
