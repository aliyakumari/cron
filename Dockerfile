FROM python:3.9

WORKDIR /cron

RUN apt-get update && apt-get install -y git

COPY ./requirements.txt /cron/requirements.txt

RUN pip install --no-cache-dir -r requirements.txt 

COPY . /cron

CMD [ "python", "./book_toc_hygiene.py", "02:55" ]
