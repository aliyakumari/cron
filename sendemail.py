import os
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.base import MIMEBase
from email import encoders
import smtplib


FROM_EMAIL = "automation-ui@embibe.com"
EMAIL_PASSWORD = "arxdplnfxknqxdex"


def check_cumulative_file_sizes(r):
    print("Checking all file sizes")
    size = 0
    try:
        for file in r:
            file = file
            size += os.stat(file).st_size

        print("Cumulative file sizes in KB: ", size / 1000)
    except Exception as e:
        print(e)
        return False
    if size > 25000000:
        return False
    return True


def attach_files(msg: MIMEMultipart, attachment_directory: str = None, attachment_files: list = None):
    try:
        files_to_attach = []
        if attachment_directory is not None:
            files_to_attach = [attachment_directory + file for file in os.listdir(attachment_directory)]

        if attachment_files is not None:
            files_to_attach += attachment_files

        print("Attaching: ", files_to_attach)
        if check_cumulative_file_sizes(files_to_attach):
            for file in files_to_attach:
                file_name = file.split('/')[-1]
                attachment = None
                try:
                    attachment = open(file, "rb")
                except Exception as e:
                    print(e)
                p = MIMEBase('application', 'octet-stream')
                p.set_payload(attachment.read())
                encoders.encode_base64(p)
                p.add_header('Content-Disposition', "attachment; filename= %s" % file, filename=file_name)

                msg.attach(p)

    except Exception as e:
        print(e)


def sendemail(email_to, subject: str, body: str = None, body_html_file_path: str = None,
              attachment_directory: str = None, attachment_files: list = None):
    """
    :param email_to: Example: aliya.kumari@embibe.com
    :param subject: Example: "Test Subject"
    :param body: Example: <html> <body> <p> This is a test body </p> </body> </html>
    :param body_html_file_path: html file path of body (Provide only one body or body_html_file_path, In case got
    both, it will consider )
    :param attachment_directory: Example: folder/
    :param attachment_files: [folder/attachment1.txt, folder/attachment2.txt]
    :return:
    """
    msg = MIMEMultipart()
    msg['From'] = FROM_EMAIL
    msg['To'] = email_to

    if attachment_directory is not None or attachment_files is not None:
        attach_files(msg, attachment_directory, attachment_files)

    msg['Subject'] = subject
    msg.attach(MIMEText(body, 'html'))

    s = smtplib.SMTP('smtp.gmail.com', 587)
    s.starttls()
    s.login(FROM_EMAIL, EMAIL_PASSWORD)
    text = msg.as_string()
    s.sendmail(FROM_EMAIL, email_to, text)
    s.quit()
    print("Email sent to " + email_to)
