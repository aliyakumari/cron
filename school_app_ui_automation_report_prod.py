import os
import time
import sys
import smtplib
import pytz
from datetime import datetime
from os import path
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.base import MIMEBase
from email import encoders
import subprocess
import glob



class Constants(object):
    def __init__(self):
        super(Constants, self).__init__()
        self._time = 'Reports generated at ' + datetime.now().strftime('%Y-%m-%d')

        self._repository = 'git clone git@bitbucket.org:microservicesembibe/school-app-ui-automation.git'
        self._directory = 'school-app-ui-automation/'
        self._command = 'cd school-app-ui-automation && mvn clean test -DdriverEnvironment=local -Denv=production -Ddriver=chrome'
        self._filename = ['target/TestReport/Test-Automaton-Report.html']
        self.result = 'target/surefire-reports/SchoolApp-report.html'
        self._from_address = 'automation-ui@embibe.com'
        self._to_address = [  "amit.ranjan@embibe.com", "subharthi.samanta@embibe.com", "vikash.singh@embibe.com", "k.badrinath@embibe.com", "rohan.lal@embibe.com","vipin.vishwakarma@embibe.com","brijesh.singh1@embibe.com"]
        self._email_subject = "School App Ui Automation Prod Report"
        self._screenshots_dir = 'target/Screenshots/'
        self._email_password = "ghvxtrtxztssyjxm"

    @property
    def repository(self):
        return self._repository

    @property
    def filename(self):
        return self._filename

    @property
    def directory(self):
        return self._directory

    @property
    def screenshots_dir(self):
        return self._screenshots_dir

    @property
    def command(self):
        return self._command

    @property
    def to_address(self):
        return self._to_address

    @property
    def from_address(self):
        return self._from_address

    @property
    def email_body(self):
        return self._email_body

    @property
    def email_subject(self):
        return self._email_subject

    @property
    def email_password(self):
        return self._email_password


class Email(object):
    def __init__(self):
        super(Email, self).__init__()
        self._to_address = None
        self._directory = None

    @property
    def directory(self):
        return self._directory

    @directory.setter
    def directory(self, directory):
        self._directory = directory

    @property
    def to_address(self):
        return self._to_address

    @to_address.setter
    def to_address(self, email):
        self._to_address = email

    def checkSize(self, r, f):
        size = 0
        try:
            for file in r:
                file = self._directory + file
                size += os.stat(file).st_size
            print
            size
        except Exception as e:
            print
            e
            return False
        if size > 25000000:
            return False
        return True

    def sendemail(self, from_address, email_password, email_subject, email_body, attachment_filename, images):
        to_address = self._to_address

        msg = MIMEMultipart()
        msg['From'] = from_address
        msg['To'] = ", ".join(to_address)
        msg['Subject'] = email_subject

        if self.checkSize(attachment_filename, images):
            for file in attachment_filename:
                attachment = None
                try:
                    attachment = open(self._directory + file, "rb")
                    print(attachment)
                except Exception as e:
                    print(e)
                p = MIMEBase('application', 'octet-stream')
                p.set_payload((attachment).read())
                encoders.encode_base64(p)
                p.add_header('Content-Disposition', "attachment; filename= %s" % file)
                msg.attach(p)

            if images is not None:
                files = images
                print(files)
                if files is not None:
                    for file in files:
                        if file == '':
                            continue
                        file_name = file.split('/')[-1]
                        part = MIMEBase('application', "octet-stream")

                        part.set_payload(open(file, "rb").read())
                        encoders.encode_base64(part)
                        part.add_header('Content-Disposition', 'attachment', filename=file_name)
                        msg.attach(part)
        else:
            email_body += '<h4>Reports did not get generated.</h4><p>The possible reasons can be :-</p>'
            email_body += '<p>1. The size of the reports exceeded the gmail attachment size limit of 25MB.</p>'
            email_body += '<p>2. The automation project did not execute as expected, hence reports were not generated.</p>'

        msg.attach(MIMEText(email_body, 'html'))
        s = smtplib.SMTP('smtp.gmail.com', 587)
        s.starttls()
        s.login(from_address, email_password)
        text = msg.as_string()
        s.sendmail(from_address, to_address, text)
        s.quit()
        print("Email sent to " + str(to_address))


class Server(object):
    def __init__(self):
        super(Server, self).__init__()

    def run(self, command):
        os.system(command)

    def checkOutput(self, path, file):
        out = ''
        try:
            out = subprocess.check_output("find " + path + " -path '" + file + "'", shell=True)
        except Exception as e:
            print(e)
        return out


class Project(object):
    def __init__(self):
        super(Project, self).__init__()
        self._server = Server()
        self._constants = Constants()
        self._email = Email()

    @property
    def server(self):
        return self._server

    @property
    def constants(self):
        return self._constants

    @property
    def email(self):
        return self._email


class Main(object):
    def __init__(self):
        super(Main, self).__init__()
        self._project = Project()

    def readFile(self, file):
        data = None
        with open(file, 'r') as f:
            print("in read file")
            data = f.read()
        return data

    def runServer(self):
        project = self._project
        project.server.run('cd ' + project.constants.directory + ' && ' + 'git checkout productionSchoolAPP')
        project.server.run(project.constants.command)
        time.sleep(5)
        project.email.to_address = project.constants.to_address
        e_from = project.constants.from_address
        e_pass = project.constants.email_password
        UTC = pytz.utc
        IST = pytz.timezone('Asia/Kolkata')
        datetime_ist = datetime.now(IST)
        e_sub = project.constants.email_subject
        e_file = project.constants.filename
        project.email.directory = project.constants.directory
        time.sleep(5)
        e_data = self.readFile("school-app-ui-automation/target/surefire-reports/SchoolApp-report.html")
        e_body = "<html><body><p><center><font color='red'><i>*****This is an auto-generated email. Please do not reply.*****</i></font></center></p><p>Hi All,<br> PFA the output files for School App UI Automation at time -"+ datetime.now().strftime('%Y-%m-%d') + e_data
        try:
            e_images = glob.glob('school-app-ui-automation/target/Screenshots/*.png')
        except Exception as e:
            print(e)
        project.email.sendemail(e_from, e_pass, e_sub, e_body, e_file, e_images)

    def schedule(self, sch):
        project = self._project
        while True:
            try:
                now = datetime.now()
                if now.hour == int(sch.split(':')[0]) and now.minute == int(sch.split(':')[1]):
                    project.server.run('sudo rm -r ' + project.constants.directory)
                    time.sleep(3)
                    project.server.run(project.constants.repository)
                    time.sleep(10)

                    self.runServer()
                time.sleep(30)
            except Exception as e:
                print(e)
                time.sleep(100)


if __name__ == '__main__':
    main = Main()
    main.schedule(sys.argv[1])